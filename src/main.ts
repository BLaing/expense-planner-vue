import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css';
import Card from 'primevue/card';
import PrimeVue from 'primevue/config';
import 'primevue/resources/primevue.min.css';
import 'primevue/resources/themes/arya-blue/theme.css';
import ToastService from 'primevue/toastservice';
import { createApp } from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import { store } from './store';

createApp(App)
  .use(PrimeVue)
  .use(ToastService)
  .use(store)
  .use(router)
  .component('pv-card', Card)
  .mount('#app');
