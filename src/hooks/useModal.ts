import { ref } from 'vue';

export const useModal = (intialState = false) => {
  const modalVisible = ref(intialState);

  const toggleModal = (value?: boolean) => {
    if (value && typeof value === 'boolean') return (modalVisible.value = value);
    modalVisible.value = !modalVisible.value;
  };

  return {
    modalVisible,
    toggleModal
  };
};
