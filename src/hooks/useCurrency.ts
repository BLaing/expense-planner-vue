import { computed } from '@vue/runtime-core';

export const useCurrency = (value: number) => {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
  });
  const computedResult = computed(() => formatter.format(value));
  return computedResult;
};
