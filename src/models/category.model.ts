export type Category = {
  id: number;
  name: string;
  budget: number;
  user_id: string;
};
