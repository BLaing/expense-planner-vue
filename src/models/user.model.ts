export type User = {
  id: string;
  email: string;
  role: string;
  aud: string;
  created_at: string;
  user_metadata: unknown;
};
