import { Category } from './category.model';

export type Transaction = {
  id: number;
  created_at: string;
  description: string;
  user_id: string;
  amount: number;
  type: 'EXPENSE' | 'INCOME';
  expensecategories?: Category;
  category_id?: number;
  date: Date | string;
};
