import Vue from 'vue';
import { StrictStore } from 'vuex';
import { Store } from './store';

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    store?: Store;
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    $store: Store;
  }
}
