import { useStore } from '@/store';
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const publicPages = ['sign-up', 'sign-in'];

const routes: Array<RouteRecordRaw> = [
  {
    path: '/signup',
    name: 'sign-up',
    component: () => import(/* webpackChunkName: "auth" */ '../views/auth/SignUp.vue')
  },
  {
    path: '/signin',
    name: 'sign-in',
    component: () => import(/* webpackChunkName: "auth" */ '../views/auth/LogIn.vue')
  },
  {
    path: '/transactions',
    name: 'transactions',
    component: () => import(/* webpackChunkName: "transactions" */ '../views/TransactionList.vue')
  },
  {
    path: '/expense-plan',
    name: 'expense-planner',
    component: () => import(/* webpackChunkName: "planner" */ '../views/ExpensePlanner.vue')
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import(/* webpackChunkName: "user" */ '../views/TheSettings.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import(/* webpackChunkName: "dashboard" */ '../views/TheDashboard.vue')
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, _, next) => {
  const store = useStore();
  if (publicPages.includes(String(to.name)) || store.getters.isLoggedIn) {
    next();
  } else {
    return next('/signin');
  }
});

export default router;
