import { createStore, Store as VuexStore, CommitOptions, DispatchOptions } from 'vuex';
import { Actions, actions } from './actions';
import { getters, Getters } from './getters';
import { Mutations, mutations } from './mutations';
import { State, state } from './state';

export const store = createStore({
  state,
  mutations,
  actions,
  getters
});

export type Store = Omit<VuexStore<State>, 'getters' | 'commit' | 'dispatch'> & {
  commit<K extends keyof Mutations, P extends Parameters<Mutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions
  ): ReturnType<Mutations[K]>;
} & {
  dispatch<K extends keyof Actions>(
    key: K,
    payload: Parameters<Actions[K]>[1],
    options?: DispatchOptions
  ): ReturnType<Actions[K]>;
} & {
  getters: {
    [K in keyof Getters]: ReturnType<Getters[K]>;
  };
};

export function useStore(): Store {
  return store as Store;
}
