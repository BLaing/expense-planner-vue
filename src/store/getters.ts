import { supabase } from '@/lib/supabase';
import { Category } from '@/models/category.model';
import { Transaction } from '@/models/transaction.model';
import { User } from '@/models/user.model';
import { GetterTree } from 'vuex';
import { State } from './state';

export type Getters = {
  currentUser(state: State): Partial<User>;
  isLoggedIn(state: State): boolean;
  transactions(state: State): Transaction[];
  categories(state: State): Category[];
  totalMonthlyBudget(state: State): number;
  monthExpenses(
    state: State
  ): { category_id: number; name: string; total: number; budget: number }[];
  currentMonth(state: State): { month: number; year: number } | undefined;
  expenseTimeline(
    state: State
  ): {
    month: string;
    category: string;
    total: number;
  }[];
};

export const getters: GetterTree<State, State> & Getters = {
  currentUser: state => {
    return state.user;
  },
  isLoggedIn: state => {
    return !!state.user.id || !!supabase.auth.user();
  },
  transactions: state => {
    return state.transactions;
  },
  categories: state => {
    return state.categories;
  },
  totalMonthlyBudget: state => {
    return state.categories.reduce((prev, curr) => prev + curr.budget, 0);
  },
  monthExpenses: state => {
    return state.monthlyData?.data || [];
  },
  currentMonth: state => {
    if (state.monthlyData) {
      const { month, year } = state.monthlyData;
      return { month, year };
    }
    return;
  },
  expenseTimeline: state => {
    return state.expenseTimeline || [];
  }
};
