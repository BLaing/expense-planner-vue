import { supabase } from '@/lib/supabase';
import { Transaction } from '@/models/transaction.model';
import { ActionContext, ActionTree } from 'vuex';
import { Mutations, MutationTypes } from './mutations';
import { State } from './state';
import dayjs from 'dayjs';
import { Category } from '@/models/category.model';

export enum ActionTypes {
  SIGN_UP = 'SIGN_UP',
  LOG_IN = 'LOG_IN',
  LOG_OUT = 'LOG_OUT',
  LOAD_TRANSACTIONS = 'LOAD_TRANSACTIONS',
  CREATE_TRANSACTION = 'CREATE_TRANSACTION',
  LOAD_CATEGORIES = 'LOAD_CATEGORIES',
  CREATE_CATEGORY = 'CREATE_CATEGORY',
  EDIT_CATEGORY = 'EDIT_CATEGORY',
  LOAD_TOTAL_PER_CATEGORY = 'LOAD_TOTAL_PER_CATEGORY',
  LOAD_EXPENSE_TIMELINE = 'LOAD_EXPENSE_TIMELINE'
}

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, 'commit'>;

export interface Actions {
  [ActionTypes.SIGN_UP](
    { commit }: AugmentedActionContext,
    payload: { email: string; password: string }
  ): Promise<void>;
  [ActionTypes.LOG_IN](
    { commit }: AugmentedActionContext,
    payload: { email: string; password: string }
  ): Promise<void>;
  [ActionTypes.LOG_OUT]({ commit }: AugmentedActionContext, payload?: unknown): Promise<void>;
  [ActionTypes.LOAD_TRANSACTIONS](
    { commit }: AugmentedActionContext,
    payload?: { year: number; month: number }
  ): Promise<void>;
  [ActionTypes.CREATE_TRANSACTION](
    { commit, getters }: AugmentedActionContext,
    payload: Partial<Transaction>
  ): Promise<void>;
  [ActionTypes.LOAD_CATEGORIES](
    { commit, getters }: AugmentedActionContext,
    payload?: unknown
  ): Promise<void>;
  [ActionTypes.CREATE_CATEGORY](
    { commit, getters }: AugmentedActionContext,
    payload: Partial<Category>
  ): Promise<void>;
  [ActionTypes.EDIT_CATEGORY](
    { commit, getters }: AugmentedActionContext,
    payload: Partial<Category>
  ): Promise<void>;
  [ActionTypes.LOAD_TOTAL_PER_CATEGORY](
    { commit, getters }: AugmentedActionContext,
    payload: { year: number; month: number }
  ): Promise<void>;
  [ActionTypes.LOAD_EXPENSE_TIMELINE](
    { commit }: AugmentedActionContext,
    payload: unknown
  ): Promise<void>;
}

export const actions: ActionTree<State, State> & Actions = {
  async [ActionTypes.SIGN_UP]({ commit }, payload) {
    const { user, error } = await supabase.auth.signUp({
      email: payload.email,
      password: payload.password
    });
    if (error) {
      throw new Error(error.message);
    }
    if (!user) {
      throw new Error('An error ocurred while creating your account. Please try again.');
    }
    commit(MutationTypes.SET_USER, user);
  },
  async [ActionTypes.LOG_IN]({ commit }, payload) {
    const { user, error } = await supabase.auth.signIn({
      email: payload.email,
      password: payload.password
    });
    if (error) {
      throw new Error(error.message);
    }
    if (!user) {
      throw new Error('An error ocurred while logging in. Please try again.');
    }
    commit(MutationTypes.SET_USER, user);
  },
  async [ActionTypes.LOG_OUT]({ commit }) {
    const { error } = await supabase.auth.signOut();
    if (error) {
      throw new Error(error.message);
    }
    commit(MutationTypes.SET_USER, {});
  },
  async [ActionTypes.LOAD_TRANSACTIONS]({ commit }, payload) {
    let query = supabase
      .from('transactions')
      .select(
        `
          id, user_id, created_at, description, amount, type, date,
          expensecategories (
            id, name, budget
          )
        `
      )
      .order('date', { ascending: false })
      .order('created_at', { ascending: false });
    let mutation = MutationTypes.ADD_TRANSACTIONS;
    if (payload) {
      const date = dayjs()
        .month(payload.month)
        .year(payload.year);
      const startOfMonth = date.startOf('month').toISOString();
      const endOfMonth = date.endOf('month').toISOString();
      query = query.gte('created_at', startOfMonth).lte('created_at', endOfMonth);
      mutation = MutationTypes.SET_TRANSACTIONS;
    }
    const { data, error } = await query;

    if (error) {
      throw new Error(error.message);
    }

    commit(mutation, data as Transaction[]);
  },
  async [ActionTypes.CREATE_TRANSACTION]({ commit }, payload) {
    const { data, error } = await supabase
      .from<Transaction>('transactions')
      .insert({ ...payload, user_id: supabase.auth.user()?.id }, { returning: 'representation' });

    if (error) {
      throw new Error(error.message);
    }

    if (!data) {
      throw new Error('Unexpected error while creating a transaction. Please try again.');
    }

    commit(MutationTypes.ADD_TRANSACTIONS, data);
  },
  async [ActionTypes.LOAD_CATEGORIES]({ commit, getters }) {
    if (getters.categories.length > 0) {
      // No need to reload categories. Won't change that often.
      return;
    }
    const { data, error } = await supabase
      .from<Category>('expensecategories')
      .select('id, name, budget');
    if (error) {
      throw new Error(error.message);
    }

    commit(MutationTypes.SET_CATEGORIES, data || []);
  },
  async [ActionTypes.CREATE_CATEGORY]({ commit, getters }, payload) {
    if (getters.categories.find((category: Category) => category.name === payload.name)) {
      throw new Error(`A category named ${payload.name} already exists.`);
    }
    const { data, error } = await supabase.from<Category>('expensecategories').insert({
      ...payload,
      user_id: supabase.auth.user()?.id
    });

    if (error || !data) {
      throw new Error(error?.message || 'Unexpected error while creating a new category.');
    }

    commit(MutationTypes.ADD_CATEGORY, data[0]);
  },
  async [ActionTypes.EDIT_CATEGORY]({ commit, getters }, payload) {
    if (
      getters.categories.find(
        (category: Category) => category.name === payload.name && category.id !== payload.id
      )
    ) {
      throw new Error(`A category named ${payload.name} already exists.`);
    }

    const { data, error } = await supabase
      .from<Category>('expensecategories')
      .update({
        name: payload.name,
        budget: payload.budget
      })
      .eq('id', payload.id || -1);

    if (error || !data) {
      throw new Error(error?.message || 'Unexpected error while creating a new category.');
    }

    commit(MutationTypes.EDIT_CATEGORY, data[0]);
  },
  async [ActionTypes.LOAD_TOTAL_PER_CATEGORY]({ commit, getters }, payload) {
    if (
      getters.currentMonth?.month === payload.month &&
      getters.currentMonth?.year === payload.year
    ) {
      return;
    }
    const date = dayjs()
      .month(payload.month)
      .year(payload.year);
    const startOfMonth = date.startOf('month').toISOString();
    const endOfMonth = date.endOf('month').toISOString();
    const { data, error } = await supabase.rpc('get_total_expenses', {
      from_date: startOfMonth,
      to_date: endOfMonth
    });

    if (error || !data) {
      throw new Error(error?.message || 'Unexpected error while fetching data.');
    }

    commit(MutationTypes.SET_MONTH_EXPENSES, {
      year: payload.year,
      month: payload.month,
      data
    });
  },
  async [ActionTypes.LOAD_EXPENSE_TIMELINE]({ commit }) {
    const { data, error } = await supabase.rpc('get_monthly_summaries');

    if (error || !data) {
      throw new Error(error?.message || 'Unexpected error while fetching data.');
    }

    commit(MutationTypes.SET_EXPENSE_TIMELINE, data || []);
  }
};
