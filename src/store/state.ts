import { Category } from '@/models/category.model';
import { Transaction } from '@/models/transaction.model';
import { User } from '@/models/user.model';

export type State = {
  user: Partial<User>;
  categories: Category[];
  transactions: Transaction[];
  monthlyData?: {
    year: number;
    month: number;
    data: { category_id: number; name: string; total: number; budget: number }[];
  };
  expenseTimeline?: {
    month: string;
    category: string;
    total: number;
  }[];
};

export const state: State = {
  user: {},
  categories: [],
  transactions: []
};
