import { Category } from '@/models/category.model';
import { Transaction } from '@/models/transaction.model';
import { User } from '@/models/user.model';
import { MutationTree } from 'vuex';
import { State } from './state';

export enum MutationTypes {
  SET_USER = 'SET_USER',
  SET_TRANSACTIONS = 'SET_TRANSACTIONS',
  ADD_TRANSACTIONS = 'ADD_TRANSACTIONS',
  SET_CATEGORIES = 'SET_CATEGORIES',
  ADD_CATEGORY = 'ADD_CATEGORY',
  EDIT_CATEGORY = 'EDIT_CATEGORY',
  SET_MONTH_EXPENSES = 'SET_MONTH_EXPENSES',
  SET_EXPENSE_TIMELINE = 'SET_EXPENSE_TIMELINE'
}

export type Mutations<S = State> = {
  [MutationTypes.SET_USER](state: S, payload: Partial<User>): void;
  [MutationTypes.ADD_TRANSACTIONS](state: S, payload: Transaction[]): void;
  [MutationTypes.SET_TRANSACTIONS](state: S, payload: Transaction[]): void;
  [MutationTypes.SET_CATEGORIES](state: S, payload: Category[]): void;
  [MutationTypes.ADD_CATEGORY](state: S, payload: Category): void;
  [MutationTypes.EDIT_CATEGORY](state: S, payload: Partial<Category>): void;
  [MutationTypes.SET_MONTH_EXPENSES](
    state: S,
    payload: {
      year: number;
      month: number;
      data: { category_id: number; name: string; total: number; budget: number }[];
    }
  ): void;
  [MutationTypes.SET_EXPENSE_TIMELINE](
    state: S,
    payload: {
      month: string;
      category: string;
      total: number;
    }[]
  ): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SET_USER](state, payload) {
    state.user = payload;
  },
  [MutationTypes.ADD_TRANSACTIONS](state, payload) {
    payload.forEach(transaction => {
      if (!state.transactions.find(({ id }) => transaction.id === id)) {
        if (!transaction.expensecategories && transaction.category_id) {
          transaction.expensecategories = state.categories.find(
            cat => cat.id === transaction.category_id
          );
        }
        state.transactions.unshift(transaction);
      }
    });
  },
  [MutationTypes.SET_TRANSACTIONS](state, payload) {
    state.transactions = payload;
  },
  [MutationTypes.SET_CATEGORIES](state, payload) {
    state.categories = payload;
  },
  [MutationTypes.ADD_CATEGORY](state, payload) {
    state.categories.push(payload);
  },
  [MutationTypes.EDIT_CATEGORY](state, payload) {
    state.categories.forEach(category => {
      if (category.id === payload.id) {
        if (!payload.name || !payload.budget) return;
        category.name = payload.name;
        category.budget = payload.budget;
        return;
      }
    });
  },
  [MutationTypes.SET_MONTH_EXPENSES](state, payload) {
    state.monthlyData = payload;
  },
  [MutationTypes.SET_EXPENSE_TIMELINE](state, payload) {
    state.expenseTimeline = payload;
  }
};
