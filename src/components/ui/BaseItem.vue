<template>
  <div class="container">
    <p class="label" v-if="label">{{ label }}</p>
    <slot></slot>
  </div>
</template>

<script lang="ts">
import { defineComponent } from 'vue';

export default defineComponent({
  props: {
    label: {
      type: String,
      required: false
    }
  }
});
</script>

<style scoped>
.p-dataview.p-dataview-list .p-dataview-content > .p-grid > div.container,
div.container {
  width: 100%;
  position: relative;
  padding: 0.85rem;
  border: 1px solid #383838;
  margin-bottom: 1rem;
}

.label {
  position: absolute;
  left: 0.15rem;
  top: -0.6rem;
  font-size: 0.8rem;
  font-weight: 300;
  margin-top: 0;
  background-color: var(--surface-100);
}
</style>
