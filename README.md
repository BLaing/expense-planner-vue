# accounter

Monthly expense planner and tracker to help keep a monthly budget.

The main purpose of building this app was to practice recently learnt skills on [Vue3](https://vuejs.org) and [Supabase](https://supabase.io).

TODOs:
* Add account management (Change password/Forgot password/etc)
* Add filters to dashboard
* Add more charts to dashboard
  * Pie chart per category/month
* Add data insights to dashboard (Ex. "You usually spend under 75% of your budget for X category")

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
